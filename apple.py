"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""

from termcolor import colored

def termcolor(clr):
     print(colored(clr, 'blue'))     

def main():
    pls = 'Пожалуйста,'
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))
    if n == 1 or n == 21:
        print(pls, n, 'яблоко')
    elif n == 2 or n == 22 or n == 3 or n == 23 or n == 4 or n == 24: 
        print(pls, n, 'яблока')
    elif n >= 5 and n <= 20 or n >= 25 and n <= 30:
        print(pls, n, 'яблок')
    else:
        print('Столько нет')




if __name__ == "__main__":
    termcolor('Grocery assistant')
    main()


